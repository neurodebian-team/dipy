From: Serge Koudoro <skab12@gmail.com>
Date: Wed, 8 Jan 2025 12:07:25 -0500
Subject: RF: update scipy.special deprecated function.

Origin: upstream, https://github.com/dipy/dipy/commit/a4fa3c2d07caa605c27cf7c3ef67d81fdff0e8b5
Forwarded: not-needed

This fixes the following warnings:

  /home/runner/work/dipy/dipy/venv/lib/python3.10/site-packages/dipy/reconst/shm.py:235: DeprecationWarning: `scipy.special.sph_harm` is deprecated as of SciPy 1.15.0 and will be removed in SciPy 1.17.0. Please use `scipy.special.sph_harm_y` instead.
    return sps.sph_harm(m_values, l_values, theta, phi, dtype=complex)

direction/tests/test_peaks.py: 2 warnings
reconst/tests/test_csdeconv.py: 1 warning
reconst/tests/test_shm.py: 24 warnings
workflows/tests/test_reconst_csa_csd.py: 9 warnings
  /home/runner/work/dipy/dipy/venv/lib/python3.10/site-packages/dipy/reconst/shm.py:909: DeprecationWarning: `scipy.special.lpn` is deprecated as of SciPy 1.15.0 and will be removed in SciPy 1.17.0. Please use `scipy.special.legendre_p_all` instead.
    legendre0 = sps.lpn(sh_order_max, 0)[0]

reconst/tests/test_csdeconv.py: 2331 warnings
  /home/runner/work/dipy/dipy/venv/lib/python3.10/site-packages/dipy/reconst/csdeconv.py:428: DeprecationWarning: `scipy.special.lpn` is deprecated as of SciPy 1.15.0 and will be removed in SciPy 1.17.0. Please use `scipy.special.legendre_p_all` instead.
    lambda z, j=j: lpn(j, z)[0][-1]

reconst/tests/test_csdeconv.py: 20 warnings
  /home/runner/work/dipy/dipy/venv/lib/python3.10/site-packages/dipy/reconst/csdeconv.py:435: DeprecationWarning: `scipy.special.lpn` is deprecated as of SciPy 1.15.0 and will be removed in SciPy 1.17.0. Please use `scipy.special.legendre_p_all` instead.
    frt[j // 2] = 2 * np.pi * lpn(j, 0)[0][-1]

reconst/tests/test_csdeconv.py: 945 warnings
  /home/runner/work/dipy/dipy/venv/lib/python3.10/site-packages/dipy/reconst/csdeconv.py:419: DeprecationWarning: `scipy.special.lpn` is deprecated as of SciPy 1.15.0 and will be removed in SciPy 1.17.0. Please use `scipy.special.legendre_p_all` instead.
    lambda z, j=j: lpn(j, z)[0][-1]

reconst/tests/test_shm.py: 2880 warnings
  /home/runner/work/dipy/dipy/venv/lib/python3.10/site-packages/dipy/reconst/tests/test_shm.py:1064: DeprecationWarning: `scipy.special.sph_harm` is deprecated as of SciPy 1.15.0 and will be removed in SciPy 1.17.0. Please use `scipy.special.sph_harm_y` instead.
    sh2 = sph_harm_sp(m_values, l_values, theta[:, None], phi[:, None])
---
 dipy/reconst/csdeconv.py       | 11 +++++++++--
 dipy/reconst/shm.py            | 25 +++++++++++++++++++++++--
 dipy/reconst/tests/test_shm.py | 10 ++++++----
 dipy/utils/compatibility.py    |  1 +
 4 files changed, 39 insertions(+), 8 deletions(-)

diff --git a/dipy/reconst/csdeconv.py b/dipy/reconst/csdeconv.py
index a03fdb0..40ab4ab 100644
--- a/dipy/reconst/csdeconv.py
+++ b/dipy/reconst/csdeconv.py
@@ -5,7 +5,7 @@ import numpy as np
 from scipy.integrate import quad
 import scipy.linalg as la
 import scipy.linalg.lapack as ll
-from scipy.special import gamma, lpn
+import scipy.special as sps
 
 from dipy.core.geometry import cart2sphere, vec2vec_rotmat
 from dipy.core.ndindex import ndindex
@@ -27,6 +27,7 @@ from dipy.reconst.shm import (
 from dipy.reconst.utils import _mask_from_roi, _roi_in_volume
 from dipy.sims.voxel import single_tensor
 from dipy.testing.decorators import warning_for_keywords
+from dipy.utils.compatibility import check_max_version
 from dipy.utils.deprecator import deprecated_params
 
 
@@ -413,11 +414,17 @@ def forward_sdt_deconv_mat(ratio, l_values, *, r2_term=False):
     sdt = np.zeros(n_orders)  # SDT matrix
     frt = np.zeros(n_orders)  # FRT (Funk-Radon transform) q-ball matrix
 
+    lpn = (
+        sps.lpn
+        if check_max_version("scipy", "1.15.0", strict=True)
+        else sps.legendre_p_all
+    )
+
     for j in np.arange(0, n_orders * 2, 2):
         if r2_term:
             sharp = quad(
                 lambda z, j=j: lpn(j, z)[0][-1]
-                * gamma(1.5)
+                * sps.gamma(1.5)
                 * np.sqrt(ratio / (4 * np.pi**3))
                 / np.power((1 - (1 - ratio) * z**2), 1.5),
                 -1.0,
diff --git a/dipy/reconst/shm.py b/dipy/reconst/shm.py
index 225b559..eb9169f 100755
--- a/dipy/reconst/shm.py
+++ b/dipy/reconst/shm.py
@@ -30,6 +30,7 @@ from dipy.core.onetime import auto_attr
 from dipy.reconst.cache import Cache
 from dipy.reconst.odf import OdfFit, OdfModel
 from dipy.testing.decorators import warning_for_keywords
+from dipy.utils.compatibility import check_max_version
 from dipy.utils.deprecator import deprecate_with_version, deprecated_params
 
 descoteaux07_legacy_msg = (
@@ -232,7 +233,20 @@ def spherical_harmonics(m_values, l_values, theta, phi, *, use_scipy=True):
     both parameters is the same as ours, with ``l >= 0`` and ``|m| <= l``.
     """
     if use_scipy:
-        return sps.sph_harm(m_values, l_values, theta, phi, dtype=complex)
+        if check_max_version("scipy", "1.15.0", strict=True):
+            return sps.sph_harm(m_values, l_values, theta, phi).astype(complex)
+        else:
+            degree = (
+                l_values.astype(int)
+                if isinstance(l_values, np.ndarray)
+                else int(l_values)
+            )
+            order = (
+                m_values.astype(int)
+                if isinstance(m_values, np.ndarray)
+                else int(m_values)
+            )
+            return sps.sph_harm_y(degree, order, phi, theta).astype(complex)
 
     x = np.cos(phi)
     val = sps.lpmv(m_values, l_values, x).astype(complex)
@@ -896,6 +910,13 @@ class QballBaseModel(SphHarmModel):
 
         """
         SphHarmModel.__init__(self, gtab)
+
+        lpn = (
+            sps.lpn
+            if check_max_version("scipy", "1.15.0", strict=True)
+            else sps.legendre_p_all
+        )
+
         self._where_b0s = lazy_index(gtab.b0s_mask)
         self._where_dwi = lazy_index(~gtab.b0s_mask)
         self.assume_normed = assume_normed
@@ -906,7 +927,7 @@ class QballBaseModel(SphHarmModel):
             sh_order_max, theta[:, None], phi[:, None]
         )
         L = -l_values * (l_values + 1)
-        legendre0 = sps.lpn(sh_order_max, 0)[0]
+        legendre0 = lpn(sh_order_max, 0)[0]
         F = legendre0[l_values]
         self.sh_order_max = sh_order_max
         self.B = B
diff --git a/dipy/reconst/tests/test_shm.py b/dipy/reconst/tests/test_shm.py
index 1383be8..a0c6264 100644
--- a/dipy/reconst/tests/test_shm.py
+++ b/dipy/reconst/tests/test_shm.py
@@ -11,7 +11,6 @@ from numpy.testing import (
     assert_equal,
     assert_raises,
 )
-from scipy.special import sph_harm as sph_harm_sp
 
 from dipy.core.gradients import gradient_table
 from dipy.core.interpolation import NearestNeighborInterpolator
@@ -171,7 +170,6 @@ def test_real_sh_descoteaux_from_index():
             message=descoteaux07_legacy_msg,
             category=PendingDeprecationWarning,
         )
-
         assert_equal(rsh(aa, bb, cc, dd).shape, (3, 4, 5, 6))
 
 
@@ -1060,8 +1058,12 @@ def test_faster_sph_harm():
         ]
     )
 
-    sh = spherical_harmonics(m_values, l_values, theta[:, None], phi[:, None])
-    sh2 = sph_harm_sp(m_values, l_values, theta[:, None], phi[:, None])
+    sh = spherical_harmonics(
+        m_values, l_values, theta[:, None], phi[:, None], use_scipy=False
+    )
+    sh2 = spherical_harmonics(
+        m_values, l_values, theta[:, None], phi[:, None], use_scipy=True
+    )
 
     assert_array_almost_equal(sh, sh2, 8)
     sh = spherical_harmonics(
diff --git a/dipy/utils/compatibility.py b/dipy/utils/compatibility.py
index 329f0b9..6f7ca74 100644
--- a/dipy/utils/compatibility.py
+++ b/dipy/utils/compatibility.py
@@ -1,4 +1,5 @@
 """Utility functions for checking different stuffs."""
+
 import operator
 
 from packaging import version
